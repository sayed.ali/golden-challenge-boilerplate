const userHandler = require('../handlers/users');

const userRouter = require('express').Router();

userRouter.post('/register', userHandler.register)

module.exports = userRouter;